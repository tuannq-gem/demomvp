//
//  UITableViewHeaderFooterViewExtention.swift
//  eMPI Vietnam
//
//  Created by gem on 12/4/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewHeaderFooterView {
    class func dequeHeaderWithTable(_ table: UITableView) -> Self {
        let nibName = String(describing: self)
        if let cell = table.dequeueReusableHeaderFooterView(withIdentifier: nibName) {
            return cell as! Self
        }

        let nib = UINib(nibName: nibName, bundle: nil)
        table.register(nib, forHeaderFooterViewReuseIdentifier: nibName)
        return initWithNibTemplate(table)
    }
    
    private class func initWithNibTemplate<T>(_ table: UITableView) -> T {
        let nibName = String(describing: self)
        let cell = table.dequeueReusableHeaderFooterView(withIdentifier: nibName)
        return cell as! T
    }
}
