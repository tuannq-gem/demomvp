//
//  UITableViewExtension.swift
//  mPI
//
//  Created by Macintosh on 6/14/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func dequeWithCell(_ cellClass: AnyClass) -> UITableViewCell {
        let nibName = String(describing: cellClass)
       
        if let cell = self.dequeueReusableCell(withIdentifier: nibName) {
            return cell
        }
        
        let bundle = Bundle.main
        let fileManege = FileManager.default
        if let pathXib = bundle.path(forResource: nibName, ofType: "nib") {
            if fileManege.fileExists(atPath: pathXib) {
                let nib = UINib(nibName: nibName, bundle: nil)
                self.register(nib, forCellReuseIdentifier: nibName)
                if let cell = self.dequeueReusableCell(withIdentifier: nibName) {
                    return cell
                }
            }
        }
        return UITableViewCell(style: .default, reuseIdentifier: "cell")
    }
    
    func resetFooterTableView() {
        self.tableFooterView = UIView(frame: CGRect.zero)
    }
    
    func removePaddingTop(_ paddingTop: CGFloat) {
        self.contentInset = UIEdgeInsets(top: paddingTop, left: 0, bottom: 0, right: 0)
    }
    func showMessageNotData() {
        let messageLabel = UILabel()
        messageLabel.textColor = UIColor.lightGray
        messageLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 17)
        messageLabel.text = "Không có dữ liệu"
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }
    
    func showMessageData(_ count: Int)  {
        if count <= 0 {
            self.showMessageNotData()
        } else {
            self.hiddenMessageNotData()
        }
    }
    
    func hiddenMessageNotData() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
    
    func reloadSection(_ section: Int) {
        self.beginUpdates()
        self.reloadSections(NSIndexSet(index: section) as IndexSet, with: .none)
        self.endUpdates()
    }
    
    func reloadRow(_ row: Int, section: Int) {
        self.beginUpdates()
        let indexPath = IndexPath(item: row, section: section)
        self.reloadRows(at: [indexPath], with: .none)
        self.endUpdates()
    }
}
