//
//  UITableViewCellExtension.swift
//  mPI
//
//  Created by Macintosh on 6/7/18.
//  Copyright © 2018 Macintosh. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell {
    class func dequeCellWithTable(_ table: UITableView) -> Self {
        let nibName = String(describing: self)
        if let cell = table.dequeueReusableCell(withIdentifier: nibName) {
            return cell as! Self
        }

        let nib = UINib(nibName: nibName, bundle: nil)
        table.register(nib, forCellReuseIdentifier: nibName)
        return initWithNibTemplate(table)
    }
    
    private class func initWithNibTemplate<T>(_ table: UITableView) -> T {
        let nibName = String(describing: self)
        let cell = table.dequeueReusableCell(withIdentifier: nibName)
        return cell as! T
    }
}
