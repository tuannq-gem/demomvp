//
//  ContentSizedTableView.swift
//  eMPI Vietnam
//
//  Created by gem on 12/16/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit

final class UITableViewContentSizeExtension : UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }

    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}
