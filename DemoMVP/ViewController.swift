//
//  ViewController.swift
//  DemoMVP
//
//  Created by iShrek on 2/2/20.
//  Copyright © 2020 iShrek. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ProductListViewProtocol {
    
    var presenter: ProductListViewPresenter!
    var productListDataSource: ProductListDataSource!
    var productListDelegate: ProductListDataDelegate!
    
    func setProductList(products: [Product]) {
        productListDataSource = ProductListDataSource()
        productListDelegate = ProductListDataDelegate()
        productListDelegate.delegate = self
        productListDataSource.products = products
        productListDelegate.products = products
        setupTable()
    }
    
    private func setupTable() {
        self.tableView.dataSource = productListDataSource
        self.tableView.delegate = productListDelegate
        self.tableView.reloadData()
    }
    

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let products = [
                   Product(name: "Keyboard", price: 6),
                   Product(name: "Mouse", price: 5),
                   Product(name: "Keycap", price: 10)
               ]
        let productListPresenter = ProductListPresenter(listView: self, products: products)
        self.presenter = productListPresenter
        self.presenter.showProductList()
    }
}

extension ViewController: ProductListDataDelegation {
    func didSelectRowAt(_ indexPath: IndexPath, _ model: Product) {
        if let _ : ProductCell = tableView.cellForRow(at: indexPath) as? ProductCell {
                print("didSelectRowAt" , model.name)
        }
    }
}

