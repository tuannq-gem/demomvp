//
//  ProductCell.swift
//  DemoMVP
//
//  Created by iShrek on 2/8/20.
//  Copyright © 2020 iShrek. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet weak var titlelb: UILabel!
    @IBOutlet weak var pricelb: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
