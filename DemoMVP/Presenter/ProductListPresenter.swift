//
//  ProductListPresenter.swift
//  DemoMVP
//
//  Created by iShrek on 2/2/20.
//  Copyright © 2020 iShrek. All rights reserved.
//

import UIKit

class ProductListPresenter: ProductListViewPresenter {
    unowned let listView: ProductListViewProtocol
    let products: [Product]
    
    
    required init(listView: ProductListViewProtocol, products: [Product]) {
        self.listView = listView
        self.products = products
    }
    
    func showProductList() {
        self.listView.setProductList(products: self.products)
    }
    
    
    
}
