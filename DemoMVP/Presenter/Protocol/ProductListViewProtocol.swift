//
//  ProductListViewProtocol.swift
//  DemoMVP
//
//  Created by iShrek on 2/2/20.
//  Copyright © 2020 iShrek. All rights reserved.
//

import Foundation
import UIKit

protocol ProductListViewProtocol: class {
    func setProductList(products: [Product])
}
