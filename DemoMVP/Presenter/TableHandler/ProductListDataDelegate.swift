//
//  ProductListDataDelegate.swift
//  DemoMVP
//
//  Created by iShrek on 2/3/20.
//  Copyright © 2020 iShrek. All rights reserved.
//

import UIKit
//import FirebaseCrashlytics

protocol ProductListDataDelegation: class {
    func didSelectRowAt(_ indexPath: IndexPath, _ model: Product)
}

class ProductListDataDelegate: NSObject, UITableViewDelegate {
    var products: [Product]!
    var delegate: ProductListDataDelegation?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        Crashlytics.crashlytics()
        let item = self.products[indexPath.row];
        delegate?.didSelectRowAt(indexPath, item)
        
        
    }
}
