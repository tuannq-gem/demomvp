//
//  ProductListDataSource.swift
//  DemoMVP
//
//  Created by iShrek on 2/3/20.
//  Copyright © 2020 iShrek. All rights reserved.
//


import UIKit

class ProductListDataSource: NSObject, UITableViewDataSource {
    var products: [Product]!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ProductCell = ProductCell.dequeCellWithTable(tableView)
        let product = products[indexPath.row]
        cell.titlelb?.text = product.name
        cell.pricelb?.text = String(product.price)
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
}
