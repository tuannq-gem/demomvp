//
//  Product.swift
//  DemoMVP
//
//  Created by iShrek on 2/2/20.
//  Copyright © 2020 iShrek. All rights reserved.
//

import Foundation

struct Product {
    var name: String
    var price: Double
}
